#include <iostream>
#include <cmath>
#include <string>

class Animal
{
public:
	virtual void Voice() = 0; // pure virtual
};

class Dog : public Animal
{
	void Voice() override
	{
		std::cout << "(dog)\tspeaking dog omg";
	}
};

class Cat : public Animal
{
	void Voice() override
	{
		std::cout << "(cat)\tMeow";
	}
};

class Rat : public Animal
{
	void Voice() override
	{
		std::cout << "(rat)\tRATatouille";
	}
};

class Lion : public Animal
{
	void Voice() override
	{
		std::cout << "(lion)\tRaw";
	}
};

int main()
{
	Animal* zoo[] = {new Cat(), new Dog(), new Rat(), new Dog(), new Lion(), new Rat(), new Cat(), new Rat()};

	for (auto animal : zoo)
	{
		animal->Voice();
		std::cout << '\n';
	}
	
	for (auto animal : zoo)
	{
		delete animal;
	}
}
