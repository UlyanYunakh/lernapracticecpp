#pragma once

template<typename T>
T SquareSum(T arg1, T arg2)
{
	return (arg1 * arg1) + 2 * arg1 * arg2 + (arg2 * arg2);
}
